package com.example.kuldeep.myresumeapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

public class MyResumeApp extends AppCompatActivity {

        TextView tv;
    Intent i;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_resume_app);
      //  tv =(TextView)findViewById(R.id.textView);
      //  tv.setText("Name : kuldeep Mourya Rajesh");

        if(getIntent().getBooleanExtra("Exit",false))
        {
            finish();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my_resume_app, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch(item.getItemId())
        {
            case R.id.Education:
                i = new Intent("Education");
                startActivity(i);
                return true;
            case R.id.Personal:
                i = new Intent("Personal");
                startActivity(i);
                return true;
            case R.id.Objective:
                i=new Intent("Objective");
                startActivity(i);
                return true;
            case R.id.Contact:
                i= new Intent("Contact");
                startActivity(i);
                return true;
            case R.id.Reference:
                i= new Intent("Reference");
                startActivity(i);
                return true;
            case R.id.Exit:
                i= new Intent(getApplicationContext(),MyResumeApp.class);
                i.setFlags(i.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra("Exit",true);
                startActivity(i);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
