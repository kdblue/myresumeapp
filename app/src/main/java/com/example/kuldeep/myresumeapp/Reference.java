package com.example.kuldeep.myresumeapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class Reference extends AppCompatActivity {

    Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reference);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_reference, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
     switch (id)
     {
         case android.R.id.home:
             onBackPressed();
             return  true;
         case R.id.myresume:
             i=new Intent(this,MyResumeApp.class);
             startActivity(i);
             return  true;
         case R.id.Education:
             i = new Intent("Education");
             startActivity(i);
             return true;
         case R.id.Personal:
             i= new Intent("Personal");
             startActivity(i);
             return true;
         case R.id.Objective:
             i=new Intent("Objective");
             startActivity(i);
             return true;
         case R.id.Contact:
             i= new Intent("Contact");
             startActivity(i);
             return true;
         case R.id.Exit:
             i= new Intent(getApplicationContext(),MyResumeApp.class);
             i.setFlags(i.FLAG_ACTIVITY_CLEAR_TOP);
             i.putExtra("Exit",true);
             startActivity(i);
             return true;
     }
        return super.onOptionsItemSelected(item);
    }
}
